#!/usr/bin/env python
# -*- coding: utf-8 -*-
from gevent import monkey
monkey.patch_all()

import argparse
import gevent
from collections import deque
from random import randint
from time import time
from amphora import (
    AmqpRpcServer, AmqpRpcClient, AmqpRpcFanoutServer, AmqpRpcFanoutClient,
    IgnoreRequest, NoResult)


parser = argparse.ArgumentParser('load_test.py')
parser.add_argument('count', type=int,
                    help='Total request count')
parser.add_argument('frequency', type=float,
                    help='Requests per second')
parser.add_argument('--timeout', type=int,
                    help='Request timeout', default=10)
parser.add_argument('--progress-modula', metavar='COUNT', type=int,
                    help='Print progress after each COUNT messages', default=None)
parser.add_argument('--no-graph', action='store_true')
parser.add_argument('--delay', type=int,
                    help='How much time request should be processed', default=0)
parser.add_argument('--servers', type=int,
                    help='Server worker processes count', default=1)
parser.add_argument('--fanout', help='Test fanout servers', action='store_true')
parser.add_argument('--queue-count', metavar='COUNT',
                    help='How many request queue should be created',
                    type=int, default=None)
args = parser.parse_args()

import logging
l = logging.getLogger('amphora')
l.setLevel(logging.WARNING)
l.addHandler(logging.StreamHandler())


def add(x, y, delay):
    ignore = x % args.servers != current_server
    if args.fanout and ignore:
        raise IgnoreRequest
    gevent.sleep(delay)
    return x + y


pids = []
for current_server in xrange(args.servers):
    pid = gevent.fork()
    if pid == 0:
        if args.fanout:
            server = AmqpRpcFanoutServer('fanoutloadtest')
        else:
            server = AmqpRpcServer('loadtest')
        try:
            server.add_function(add)

            def test_listen_from_queue(count):
                from random import randint
                gevent.sleep(1)
                while True:
                    modula = randint(0, count - 1)
                    print 'Consume mod{0} for 1000 requests'.format(modula)
                    start = time()
                    try:
                        consumed = server.receive_from_queue(
                            'mod' + str(modula), timeout=1,
                            block=True, max_calls=1000)
                    except gevent.Timeout:
                        consumed = 0
                    end = time()
                    print ('Stop consume mod{1}. Comsumed {2} messages '
                           'for {0} seconds').format(
                               end - start, modula, consumed)
                    # gevent.sleep(0.5)
            if args.queue_count is not None:
                gevent.spawn(test_listen_from_queue, args.queue_count)

            server.serve(nowait=False)
        finally:
            server.stop()
        break
    else:
        pids.append(pid)
else:
    try:
        if args.fanout:
            client = AmqpRpcFanoutClient('fanoutloadtest', uuid='client')
        else:
            client = AmqpRpcClient('loadtest')
        if args.queue_count is not None:
            for modula in xrange(args.queue_count):
                client.create_new_request_queue('mod' + str(modula), nowait=True)

        count = args.count
        frequency = args.frequency
        timeout = args.timeout
        queue_count = args.queue_count
        progress_modula = args.progress_modula
        delay = args.delay
        if progress_modula is None:
            progress_modula = count / 100
        progress_modula = max(progress_modula, 1)
        period = 1.0 / frequency
        times = deque()

        def exec_query(number, args):
            args = list(args)
            if number % progress_modula == 0:
                print number
            start = time()
            if queue_count is not None:
                routing_key = 'mod' + str(args[0] % queue_count)
            else:
                routing_key = ''
            try:
                result = client.call(timeout=timeout, routing_key=routing_key
                                 ).add(*args)
            except NoResult:
                pass
            else:
                end = time()
                assert result == sum(args[:2])
                times.append(end - start)

        queries = [(randint(0, 9999), randint(0, 9999), delay) for _ in xrange(count)]
        gevent.sleep(1)
        gevent.joinall([
                gevent.spawn_later(number * period, exec_query, number, a)
                for number, a in enumerate(queries)])
    finally:
        import os
        for pid in pids:
            os.kill(pid, 15)

    print 'Finished. Successed: {0} from {1} ({2:.4}%)'.format(
        len(times), len(queries), 100.0 * len(times) / len(queries))
    client.stop()

    if not args.no_graph:
        from matplotlib import pyplot
        title = ("[{fanout}] {total} requests, {frequency} requests per second, "
                 "{servers} server workers".format(
                total=count, frequency=frequency, servers=args.servers,
                fanout=["DIRECT", "FANOUT"][args.fanout]))
        fig1 = pyplot.figure(title)
        part1 = fig1.add_subplot(211, xlabel="Seconds being processed", ylabel="Count of requests",
                                 title=title)
        part1.hist(times, bins=50, facecolor="green")
        part2 = fig1.add_subplot(212, xlabel="Request No", ylabel="Seconds being processed")
        part2.plot(range(len(times)), times)
        pyplot.show()
    if len(times) != len(queries):
        exit(1)
