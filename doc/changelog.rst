Release 0.1.3 (2013-06-11)
**************************

* Major changes in request queue algorithms
* ``FailsafeAmqpConsumer.start_consume`` return consumer tag
* ``FailsafeAmqpConsumer.stop_consume`` requires tag instead of queue name
* Support for Queue TTL (RabbitMQ extension)
* Lots of bugfixes


Release 0.1.2  (2013-05-29)
***************************

* Added ``AmqpRpcServer.prepare_stop``.
* Fixed performance issue with AMQP transactions.
* ``AmqpRpcServer.receive_from_queue`` raises ``ValueError`` if queue does not exists.


Release 0.1.1  (2013-05-22)
***************************

* Creating/deleting queues/exchanges, consuming/cancelling and publishing message operations now can block greenlet.
* By default ``AmqpRpcClient.defer`` blocks greenlet until request message published.
* Creating/deleting queues/exchanges by default block greenlet.
