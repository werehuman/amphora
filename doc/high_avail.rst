===================
 About reliability
===================

Amphora allows you to shut down some components of your system for short maintenance. The system holds its current state and waits until all components start to work again. But it is not possible to save all messages in every fantastic or real case and there are some restrictions about.

Shutting down the server
========================

If you want to immediately shut down one server then all requests will be requeued by AMQP server and sent to another server.

.. digraph:: server

   rankdir=RL;
   subgraph servers {
     rank=same;
     server1 [label="R̶P̶C̶ ̶S̶e̶r̶v̶e̶r̶", shape=box, color=red];
     server2 [label="RPC Server", shape=box];
     }
   amqp [label="AMQP Server", shape=ellipse];
   client [label="RPC Client", shape=box];

   client -> amqp [label="1. Send request"];
   amqp -> server1 [label="2. Send request"];
   server1 -> amqp [label="3. Server disconnected.\nTry another server.",
       style=dashed, arrowhead=onormal];
   amqp -> server2 [label="4. Send request to\nanother server"];
   server2 -> amqp [label="5. Response"];
   amqp -> client [label="6. Response"];

When there is only one server, client will wait until the server start up.

.. digraph:: server

   rankdir=LR;
   server [label="R̶P̶C̶ ̶S̶e̶r̶v̶e̶r̶", shape=box, color=red];
   amqp [label="AMQP Server", shape=ellipse];
   client [label="RPC Client", shape=box];

   client -> amqp [label="Send request"];
   amqp -> server [label="Holds request until server is up", style=dashed, arrowhead=onormal];

.. note::

   If you terminate the server when it handles requests, then
   those request will be executed again when server will start up.
   Be careful with side effects of request handlers.


Shutting down the client
========================

When your client loses network connection with AMQP server, client will receive responses when network fixes. But if you manually shut down your client application, your responses will be lost. It is like to terminate application when it stores records in database (without transactions): records will be created in the database but client will not know about results of this operation because client is dead.

Keep in mind that remote function call result can be delivered only to client that sent those request.

.. digraph:: client

   size="10.0"; rankdir=RL;
   server [label="RPC Server", shape=box];
   amqp [label="AMQP Server", shape=ellipse];
   subgraph clients {
     rank=same;
     client1 [label="R̶P̶C̶ ̶C̶l̶i̶e̶n̶t̶", shape=box, color=red];
     client2 [label="RPC Client", shape=box];
     }

   client1 -> amqp [label="1. Send request"];
   amqp -> server [label="2. Send request"];
   server -> amqp [label="3. Send response"];
   amqp -> client1 [label="4. Wait until client\nconnects to server\nand send response", style=dashed, arrowhead=onormal]


Shutting down the AMQP server
=============================

Main feature is that you will not lose your requests and responses even if you send `kill -9` to AMQP server. While AMQP server is down, both client and server holds outgoing messages. Client and server will authomatically reconnects to AMQP server when it starts.

.. warning::

   When you restart AMQP server, you will not lose requests
   but each pending request can be executed twice.
