=====
 API
=====

Server API
**********

.. autoclass:: amphora.AmqpRpcServer
   :members: add_function, receive_from_queue, serve, prepare_stop, stop, stop_publisher, stop_consumer
.. autoclass:: amphora.AmqpRpcFanoutServer

Client API
**********

.. autoclass:: amphora.AmqpRpcClient
   :members: defer, call, create_new_request_queue, remove_request_queue, tune_function, serve, stop, stop_publisher, stop_consumer
.. autoclass:: amphora.AmqpRpcFanoutClient

Exceptions
**********

.. autoexception:: amphora.NoResult
.. autoexception:: amphora.WrongRequest
.. autoexception:: amphora.WrongResult
.. autoexception:: amphora.RemoteException
.. autoexception:: amphora.IgnoreRequest
