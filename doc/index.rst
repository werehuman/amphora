.. amphora documentation master file, created by
   sphinx-quickstart on Sun May 19 11:57:57 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to amphora's documentation!
===================================

Amphora is a Python library for asynchronous remote procedure executing via AMQP protocol. Amphora actively uses `gevent <http://gevent.org>`_ and requires for monkey-patching Python sockets with gevent.

Amphora works with Python 2.6 and 2.7 and was tested with RabbitMQ 2.7, 3.0 and 3.1.

Key features of Amphora:

* Reliability. You will never lose any request or response accidentally even if you suddenly lose network connection for a while.
* Designed for using with `gevent <http://gevent.org>`_ and greenlets (similar with Erlang actors).
* RPC server can subscribe and unsubscribe from different request queues on the fly. Limiting subscriptions by maximum requests and/or by timeout.

Contents:

.. toctree::
   :maxdepth: 2

   high_avail
   tutorial
   api

:doc:`Changelog <changelog>`

Indices and tables
==================

* :ref:`genindex`
